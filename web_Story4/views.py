from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
def Home(request):
    return render(request, 'HomePage.html')

def Contact(request):
    return render(request, 'ContactPage.html')

def Journal(request):
    return render(request, 'JournalPage.html')

def Form(request):
    return render(request, 'FormPage.html')

def Portfolio(request):
    return render(request, 'PortfolioPage.html')

def AboutMe(request):
    return render(request, 'AboutMePage.html')
