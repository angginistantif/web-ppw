from django.urls import path
from . import views
#url for app
urlpatterns = [
    path('Home/', views.Home, name='Home'),
    path('Contact/', views.Contact, name='Contact'),
    path('Journal/', views.Journal, name='Journal'),
    path('Form/', views.Form, name='Form'),
    path('Portfolio/', views.Portfolio, name='Portfolio'),
    path('AboutMe/', views.AboutMe, name='AboutMe'),
]
