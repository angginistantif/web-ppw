"""web_ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.urls import re_path, include
from django.conf.urls import url, include
from web_Story4.views import Home as Home
from web_Story4.views import Contact as Contact
from web_Story4.views import Journal as Journal
from web_Story4.views import Form as Form
from web_Story4.views import Portfolio as Portfolio
from web_Story4.views import AboutMe as AboutMe
from web_Story5.views import membuatJadwal as DataActivity 
from web_Story5.views import jadwalList as ActivityPage
from web_Story5.views import deleteList 


urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^HomePage.html', Home, name="Home"),
    re_path(r'^ContactPage.html', Contact, name="Contact"),
    re_path(r'^JournalPage.html', Journal, name="Journal"),
    re_path(r'^FormPage.html', Form, name="Form"),
    re_path(r'^PortfolioPage.html', Portfolio, name="Portfolio"),
    re_path(r'^AboutMePage.html', AboutMe, name="AboutMe"),
    re_path(r'^DataActivity.html', DataActivity, name="DataActivity"),
    re_path(r'^ActivityPage.html', ActivityPage, name="ActivityPage"),
    re_path(r'^deleteList', deleteList, name="deleteList"),
    
]
