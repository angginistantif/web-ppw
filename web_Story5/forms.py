from django.forms import ModelForm
from .models import jadwalKegiatanKu
from django import forms

class jadwalKegiatanForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(jadwalKegiatanForm, self).__init__(*args, **kwargs)

        self.fields['name'].label = 'Nama Kegiatan'
        self.fields['name'].widget.attrs = {'class': 'form-control', 'placeholder': 'Masukkan nama kegiatan'}
        self.fields['name'].error_messages = {'max_length': 'Maksimal karakter adalah 200 karakter.'}

        self.fields['place'].label = 'Tempat Kegiatan'
        self.fields['place'].widget.attrs = {'class': 'form-control', 'placeholder': 'Masukkan tempat kegiatan'}
        self.fields['place'].error_messages = {'max_length': 'Maksimal karakter adalah 200 karakter.'}

        self.fields['category'].label = 'Kategori Kegiatan'
        self.fields['category'].widget.attrs = {'class': 'form-control', 'placeholder': 'Masukkan kategori kegiatan'}
        self.fields['category'].error_messages = {'max_length': 'Maksimal karakter adalah 100 karakter.'}
        self.fields['time'] = forms.DateTimeField( widget=forms.DateTimeInput(attrs={'type': 'datetime-local','class': 'form-control'}),
                                                    label='Waktu Kegiatan', input_formats=['%d/%m/%Y %H:%M'],
                                                    error_messages={'invalid': 'Format tanggal dan waktu tidak valid.'})
    class Meta:
        model = jadwalKegiatanKu
        fields = ['name', 'place', 'category', 'time']
