from django import forms
from django.db import models
import datetime

class jadwalKegiatanKu (models.Model):
    name = models.CharField(max_length=200)
    place = models.CharField(max_length=200)
    category = models.CharField(max_length=100)
    time = models.DateTimeField()

    def __str__(self):
        return self.name
#        return self.message
