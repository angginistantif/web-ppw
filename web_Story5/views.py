from django.urls import reverse
from .forms import jadwalKegiatanForm
from .models import jadwalKegiatanKu
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import CreateView#, ListView

from django.contrib import messages
from django.shortcuts import render, render_to_response
from django.template import RequestContext, loader

def membuatJadwal(request):
    if request.method == "POST" :
        form = jadwalKegiatanForm(request.POST or None)
        if form.is_valid():
            form.save()
    else:
        form = jadwalKegiatanForm()
    t = loader.get_template('DataActivity.html')
    c = {'form': form,}
    return render(request, 'DataActivity.html', c)
	
def jadwalList(request):
    form = jadwalKegiatanForm(request.POST or None)
    if(request.method == 'POST'):
        name = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        place = request.POST['place'] if request.POST['place'] != "" else "Anonymous"
        category = request.POST['category']
        time = request.POST['time']
        message = jadwalKegiatanKu(name=name, place=place, category=category, time=time)
        message.save()

        html = jadwalKegiatanKu
        template_name = 'ActivityPage.html'
        context_object_name = 'schedule'
        queryset = html.objects.all().values()
        result = {context_object_name : queryset}
        return render(request, template_name, result)

    else:
        model = jadwalKegiatanKu
        template_name = 'ActivityPage.html'
        context_object_name = 'schedule'
        queryset = model.objects.all().values()

        result = {context_object_name : queryset}
        return render(request, template_name, result)

def deleteList(request):
    jadwalKegiatanKu.objects.all().delete()
    context ={}
    return render(request, 'ActivityPage.html', context)

# class membuatJadwal(CreateView):
#     model = jadwalKegiatanKu
#     form_class = jadwalKegiatanForm
#     template_name = 'jadwalKegiatan.html'
    
#     def get_success_url(self):
#         return reverse('schedule:jadwalKegiatanLists')
