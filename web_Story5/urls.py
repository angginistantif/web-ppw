from django.urls import re_path

from .views import membuatJadwal, jadwalList    

urlpatterns = [
    re_path(r'^DataActivity.html', membuatJadwal, name= "DataActivity"),
    re_path(r'^ActivityPage.html', jadwalList, name= "ActivityPage"),
]
